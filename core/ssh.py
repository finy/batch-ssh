#!/usr/bin/env python
#coding:utf8
# date 2014.03.20
# author:finy


import sys
import time
import socket
import traceback
from decimal import Decimal
from decimal import getcontext
try:
    from paramiko import SSHClient
    from paramiko import AutoAddPolicy, RSAKey, PasswordRequiredException, \
        AuthenticationException
except AttributeError:
    print "import Module failure"
    print "Please run:"
    print "           pip install pycrypto==2.6.1"
    exit(1)
except ImportError:
    print "[Error] paramiko Module Not install, Please install, run:"
    print " " * 10, "pip install paramiko"
    exit(1)


getcontext().prec = 4

class ssh(SSHClient):
    def __init__(self):
        SSHClient.__init__(self)
        self.login_status = None
        self.sftp_transferred = 0
        self.sftp_remotepath = ''
        self.sftp_transferred_lasttime = 0
        self.sftp_speed = 0

    def login(self, ip, user, passwd='', port=22, key=None, key_pass=None):
        'login to ssh server'
        self.set_missing_host_key_policy(AutoAddPolicy())
        if key:
            try:
                key_file = RSAKey.from_private_key_file(key)
            except PasswordRequiredException:
                key_file = RSAKey.from_private_key_file(key, key_pass)
        else:
            key_file = None
        try:
            self.connect(ip, port, user, passwd, pkey=key_file)
            self.login_status = True
        except AuthenticationException:
            print "Error: user or password error, info: ", \
                "ip:", ip, self.error_get()[1]
            self.login_status = False
        except socket.error:
            print "Error: do't connect to host: %s, info: %s " % \
                (ip, str(self.error_get()[1]))
            self.login_status = False
        except Exception:
            self.login_status = False
            print "Error: host: %s unknow errow, info: %s" % (
                ip, str(self.error_get()[1]))

    def run_cmd(self, command, write=None):
        'run command and return stdout'
        if self.login_status:
            try:
                stdin, stdout, stderr = self.exec_command(command, get_pty=True)
                if write:
                    stdin.write(write)
                    stdin.flush()
                out = stdout.read()
                err = stderr.read()
                if all([out, err]):
                    out, status = (out + err, True)
                elif out:
                    out, status = (out, True)
                elif err:
                    out, status = (err, False)
                else:
                    out, status = ('', False)
                return out, status
            except:
                return "Error: " + str(self.error_get(out=True)[1]), False

    def sftp_get(self, remotepath, localpath):
        'sftp get file remotepathfile localpathfile'
        if self.login_status:
            sftpclient = self.open_sftp()
            try:
                self.sftp_remotepath = localpath
                sftpclient.get(remotepath, localpath, callback=self.show_transferre)
                return True
            except:
                print traceback.print_exc()
                #self.error_get(out=True)
                return False

    def size_to_huma(self, size):
        k1 = 1024
        m1 = k1 * 1024
        g1 = m1 * 1024
        
        if size >= g1:
            return '{0}GB'.format(size / 1024 / 1024 / 1024)
        elif size >= m1:
            return '{0}MB'.format(size / 1024 / 1024)
        elif size >= k1:
            return '{0}KB'.format(size / 1024)
        else:
            return '{0}B'.format(size)       

    def show_transferre(self, transferred, toBeTransferred):
        'sftp put get progress'
        if (time.time() - self.sftp_transferred_lasttime) >= 1:
            self.sftp_speed = transferred - self.sftp_transferred
            self.sftp_transferred_lasttime = time.time()
            self.sftp_transferred = transferred

        c = '{0}\t{1}\t{2}\t{3}/s\t{4}%\r'.format(self.sftp_remotepath, 
            self.size_to_huma(toBeTransferred), self.size_to_huma(transferred),
            self.size_to_huma(self.sftp_speed), (Decimal(transferred) / (Decimal(toBeTransferred) / 100)))
        sys.stdout.write(c)
        sys.stdout.flush()

    def sftp_put(self, localpath, remotepath):
        'sftp put file  localpathfile remotepathfile'

        if self.login_status:
            sftpclient = self.open_sftp()
            try:
                self.sftp_remotepath = remotepath
                self.transferred_time = time.time()
                sftpclient.put(localpath, remotepath, callback=self.show_transferre)
                return True
            except:
                print traceback.print_exc()
                #self.error_get(out=True)
                return False

    def error_get(self, out=False):
        ty, va, tr = sys.exc_info()
        if out:
            print ty.__name__, ":", va
        return ty, va, tr

    def close(self):
        self.login_status = False
        super(ssh, self).close()
